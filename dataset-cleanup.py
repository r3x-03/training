import os
from shutil import copyfile
f = open("dataset-cleanup.txt", "r")

SOURCE = "/media/harvsftw/30DA416FDA413300/rice-diseases-image-dataset/LabelledRice/Labelled/Healthy"
DESTINATION = "/home/harvsftw/Desktop/dataset"
for file_directory in (f.read().split("\n")):
    file_name = os.path.split(file_directory)
    
    if len(file_name) == 0:
    	continue
    	
    file_name = file_name[len(file_name)-1] #last part of directory
    
    try:
    	copyfile(os.path.join(SOURCE, file_name), os.path.join(DESTINATION, file_name))
    except:
    	print("Error copying ", file_name)
    
