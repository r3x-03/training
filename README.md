# Assessment of presence of rice blast disease using a convolutional neural network

## Setup
### Prerequisites
1. Python or Anaconda Prompt
2. imgaug, matplotlib, numpy, pandas, sklearn, tensorflow
3. CUDA (for NVIDIA)
### Obtaining the dataset
1. Download the dataset from Google Drive
2. Change `SOURCE` in `dataset-cleanup.py` to the location of the `Healthy` part of the dataset
3. Change `DESTINATION` in `dataset-cleanup.py` to any new folder
4. Run `python3 dataset-cleanup.py`
5. Replace `Healthy` part of dataset with the cleaned-up version
6. Create a new folder for the processed version of the dataset.
7. Inside the new folder create folders `Healthy` and `LeafBlast`
8. Set the first `TRAIN_DATA_DIR` to `<original dataset location>/Healthy`
9. Set the first `TRAIN_DATA_DIR_FIXED` to `<new folder location>/Healthy`
10. Set the second `TRAIN_DATA_DIR` to `<original dataset location>/LeafBlast`
11. Set the second `TRAIN_DATA_DIR_FIXED` to  `<new folder location>/LeafBlast`
12. Run `python3 awb.py`


## Usage
### Training the model
1. Change `TRAIN_DATA_DIR` (in `training.py`) to the location of the processed dataset (`<new folder location>`)
2. Run `python3 training.py`

### Conversion to TensorFlow Lite (for Android)
1. Run `python3 tflite-converter.py`

### Advanced usage of `training.py`
1. `run(mode=MODE_WARMUP)` - trains a classifier from randomly initialized weights. This is important as randomly initialized weights can cause fine-tuning to fail ([source](https://github.com/keras-team/keras-io/blob/master/guides/transfer_learning.py#L257))
2. `run(mode=MODE_TRAIN)` - performs fine-tuning
3. `run(mode=MODE_RESUME)` - (optional) resumes fine-tuning of a previously fine-tuned model.
4. `test()` - runs neural network on test dataset, displays ROC/AUC, and saves predictions to `output.txt`

