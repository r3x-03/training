'''
Notes for Windows to avoid weird errors:

Download numpy, scipy, h5py, and matplotlib from https://www.lfd.uci.edu/~gohlke/pythonlibs/
Then install using pip3 install file

OR

Use Anaconda prompt
'''

##################################################################
## -------------------- IMPORTANT SETTINGS -------------------- ##
##################################################################

# Notes for AMD/Intel GPUs:
# Set AMD_HARDWARE to enable alternative backend with AMD/Intel GPU support
AMD_HARDWARE = False

# locations of dataset files
# code will automatically handle splitting to train and validation
TRAIN_DATA_DIR = "/home/harvsftw/Desktop/dataset-awb"

##################################################################
## ------------------ LESS IMPORTANT SETTINGS ----------------- ##
##################################################################

# dimensions of our images (in pixels)
IMG_WIDTH  = 512
IMG_HEIGHT = 512

# seed for train/validation randomizer
SEED = 1337

# number of categories (diseases + healthy)
CLASSES = ['Healthy', 'LeafBlast']
#CLASSES = ['Healthy', 'LeafBlast', 'BrownSpot', 'Hispa']
CATEGORIES = len(CLASSES)

# batch size
batch_size = 16

# Pick one architecture
#arch = "vgg16"
#arch = "densenet"
arch = "resnet50"
#arch = "inceptionV3"

# Constants
MODE_WARMUP = 0
MODE_TRAIN  = 1
MODE_RESUME = 2 # resume from saved

# path to the "warmed-up" weights
WARMUP_WEIGHTS_PATH = 'warmup_' + arch + '.h5'

# path to fully-trained weight
SAVE_PATH = "save1"
SAVE_WEIGHTS_PATH = "save2"

from imgaug import augmenters as iaa
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import random
if not AMD_HARDWARE:
    # Keras using Tensforflow (NVDIA only) backend
    from tensorflow import keras
    from tensorflow.keras.layers import Dropout, Flatten, Dense, MaxPool2D, GlobalAveragePooling2D, GlobalMaxPooling2D
else:
    # Keras using PlaidML (by Intel) backend
    os.environ["KERAS_BACKEND"] = "plaidml.keras.backend"
    import keras
    from keras.layers import Dropout, Flatten, Dense, MaxPool2D, GlobalAveragePooling2D, GlobalMaxPooling2D

# experimental
from sklearn import metrics

# get function needed to convert images to specific neural network's format
def getPreprocessBaseModel(x, data_format=None):
    if arch == "vgg16":
        return keras.applications.vgg16.preprocess_input(x, data_format=None)
    elif arch == "densenet":
        return keras.applications.densenet.preprocess_input(x, data_format=None)
    elif arch == "resnet50":
        return keras.applications.resnet50.preprocess_input(x, data_format=None)
    elif arch == "inceptionV3":
        return keras.applications.inception_v3.preprocess_input(x, data_format=None)
        
def getPreprocess(x, data_format=None):
    # format conversion
    x = x.astype(np.uint8)
    
    # https://imgaug.readthedocs.io/en/latest/source/examples_basics.html
    augmentation = iaa.Sequential([
        # horizontal and vertical flips
        iaa.Fliplr(0.5), 
        iaa.Flipud(0.5),
        
        # random crops
        iaa.Crop(percent=(0, 0.1)), 
        
        # Small gaussian blur with random sigma between 0 and 0.5.
        # But we only blur about 50% of all images.
        iaa.Sometimes(
            0.5,
            iaa.GaussianBlur(sigma=(0, 0.5))
        ),
        
        # Strengthen or weaken the contrast in each image.
        iaa.LinearContrast((0.75, 1.5)),
        
        # Add gaussian noise.
        # For 50% of all images, we sample the noise once per pixel.
        # For the other 50% of all images, we sample the noise per pixel AND
        # channel. This can change the color (not only brightness) of the
        # pixels.
        iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5),
        
        # Make some images brighter and some darker.
        # In 20% of all cases, we sample the multiplier once per channel,
        # which can end up changing the color of the images.
        iaa.Multiply((0.8, 1.5), per_channel=0.2),
        
        # 4500K <-> 6500K <-> 8500K
        iaa.ChangeColorTemperature([4500, 8500]),
        iaa.AddToSaturation((-75, 75)),
        
        # Apply affine transformations to each image.
        # Scale/zoom them, translate/move them, rotate them and shear them.
        iaa.Affine(
            scale={"x": (0.8, 1.5), "y": (0.8, 1.5)},
            translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
            rotate=(-180, 180),
            shear=(-8, 8),
        ),
    ], random_order=True) # apply augmenters in random order
    
    return getPreprocessBaseModel(augmentation.augment_image(x), data_format=data_format)

#https://sthalles.github.io/keras-regularizer/
import tempfile
def add_regularization(model, regularizer=keras.regularizers.l2(0.0001)):

    if not isinstance(regularizer, keras.regularizers.Regularizer):
      print("Regularizer must be a subclass of tf.keras.regularizers.Regularizer")
      return model

    for layer in model.layers:
        for attr in ['kernel_regularizer']:
            if hasattr(layer, attr):
              setattr(layer, attr, regularizer)

    # When we change the layers attributes, the change only happens in the model config file
    model_json = model.to_json()

    # Save the weights before reloading the model.
    tmp_weights_path = os.path.join(tempfile.gettempdir(), 'tmp_weights.h5')
    model.save_weights(tmp_weights_path)

    # load the model from the config
    model = keras.models.model_from_json(model_json)
    
    # Reload the model weights
    model.load_weights(tmp_weights_path, by_name=True)
    return model
    
def run(mode, epochs):
    inputs = keras.Input(shape=(IMG_WIDTH, IMG_HEIGHT, 3))
        
    # Load architecture
    if arch == "vgg16":
        base_model = keras.applications.VGG16
    elif arch == "densenet":
        base_model = keras.applications.DenseNet121
    elif arch == "resnet50":
        base_model = keras.applications.ResNet50
    elif arch == "inceptionV3":
        base_model = keras.applications.InceptionV3
    
    base_model = base_model(weights='imagenet', include_top=False, input_shape=(IMG_WIDTH, IMG_HEIGHT, 3))
    
    # test for resnet50
    base_model = add_regularization(base_model)
    print(base_model.losses) # dump list of applied regulizers
    
    if arch == "vgg16":
        model = (base_model(inputs))
    else:
        model = (base_model(inputs, training=False)) # training=False from keras docs https://keras.io/guides/transfer_learning/
        

    # VGG16: replace 224x224 pooling with 448x448
    if arch == "vgg16":
        model.add(MaxPool2D(pool_size=(IMG_WIDTH // 224, IMG_WIDTH // 224), strides=(IMG_WIDTH // 224, IMG_WIDTH // 224),
                            input_shape=model.output_shape[1:])) #integer division
                            
    # Fully connected layer
    if arch == "vgg16":
        model = Flatten()(model)
        model = Dense(1024, activation='relu')(model)
        model = Dropout(0.5)(model)
        model = Dense(CATEGORIES, activation='softmax')(model)
        model = keras.Model(inputs, model)
    elif arch == "densenet":
        model = GlobalAveragePooling2D()(model)
        model = Dense(CATEGORIES, activation='softmax')(model)
        model = keras.Model(inputs, model)
    elif arch == "resnet50":
        model = GlobalAveragePooling2D()(model)
        model = Dense(1024, activation='relu')(model)
        model = Dropout(0.5)(model)
        model = Dense(CATEGORIES, activation='softmax')(model)
        model = keras.Model(inputs, model)
    elif arch == "inceptionV3":
        # Source https://keras.io/api/applications/#inceptionv3
        model = GlobalAveragePooling2D()(model)
        model = Dense(1024, activation='relu')(model)
        model = Dropout(0.5)(model)
        model = Dense(CATEGORIES, activation='softmax')(model)
        model = keras.Model(inputs, model)
    
    # Debug print
    model.summary()
    
    # test
    model = add_regularization(model)

    # "Freeze" neural network layers
    base_model_layers = model.layers[1].layers

    if mode == MODE_WARMUP:
        # In warmup mode we only train the fully connected layer
        for layer in base_model_layers:
            layer.trainable = False
    else:

        # Freeze all layers to allow saved warmup file to load
        for layer in base_model_layers:
            layer.trainable = False
        
        # Load warmed up weights
        # For resume mode unfreeze/freeze layers first before loading
        if mode == MODE_TRAIN:
            model.load_weights(WARMUP_WEIGHTS_PATH)
        
        # Unfreeze all layers
        for layer in base_model_layers:
            layer.trainable = True

        if arch == "vgg16":
            # Unfreeze last 8 layers
            unfreeze = 8
        elif arch == "densenet":
            # Not sure sa value
            unfreeze = 282
        elif arch == "resnet50":
            # Unfreeze last 94 layers (if not working change to 32)
            CONFIG_RESNET50_FREEZE_CONV1_ONLY = 167 # freeze conv1
            CONFIG_RESNET50_FREEZE_CONV1_TO_2 = 136 # freeze conv1 conv2
            CONFIG_RESNET50_FREEZE_CONV1_TO_3 = 94  # freeze conv1 conv2 conv3
            CONFIG_RESNET50_FREEZE_CONV1_TO_4 = 32  # freeze conv1 conv2 conv3 conv4
            CONFIG_RESNET50_FREEZE_CONV1_TO_5 = 0   # freeze conv1 conv2 conv3 conv4 conv5
            unfreeze = CONFIG_RESNET50_FREEZE_CONV1_TO_2
        elif arch == "inceptionV3":
            # Freze first 249 layers
            freeze = 249 # https://keras.io/api/applications/#inceptionv3
            
        if arch == "inceptionV3":
            # Freeze first 249 layers
            for layer in base_model_layers[:freeze]:
                layer.trainable = False
        else:
            # Freeze all layers except for the last n layers
            for layer in base_model_layers[:-unfreeze]:
                layer.trainable = False

            # Debugging
            for layer in base_model_layers:
                if layer.trainable:
                    isTrainable = "is trainable"
                else:
                    isTrainable = "is not trainable"
                print(layer.name, isTrainable)
        
        # Load trained model only after freezing/unfreezing layers
        if mode == MODE_RESUME:
            model.load_weights(SAVE_WEIGHTS_PATH)

    # https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html
    if mode == MODE_WARMUP:
        adam = keras.optimizers.Adam()
    else:
        adam = keras.optimizers.Adam(1e-5)
    model.compile(optimizer=adam, loss='categorical_crossentropy', metrics=['accuracy'])


    # Load dataset
    train_datagen = keras.preprocessing.image.ImageDataGenerator(preprocessing_function=getPreprocess)
    validation_datagen = keras.preprocessing.image.ImageDataGenerator(preprocessing_function=getPreprocessBaseModel)
        
    train_generator = validation_datagen.flow_from_dataframe(
        train_df,
        target_size=(IMG_HEIGHT, IMG_WIDTH),
        batch_size=batch_size,
        class_mode='categorical',
        classes=CLASSES)

    validation_generator = validation_datagen.flow_from_dataframe(
        validation_df,
        target_size=(IMG_HEIGHT, IMG_WIDTH),
        batch_size=batch_size,
        class_mode='categorical',
        classes=CLASSES)

    # Fine-tune the model
    history = model.fit_generator(
        train_generator,
        epochs=epochs,
        validation_data=validation_generator, workers=4)

    # Display training result graph and save results
    if mode != MODE_WARMUP:
        # Save weights
        model.save_weights(SAVE_WEIGHTS_PATH)
        model.save(SAVE_PATH)
        
        try:
            # List all data in history
            print(history.history.keys())
            
            # Summarize history for accuracy
            plt.plot(history.history['accuracy'])
            plt.plot(history.history['val_accuracy'])
            plt.title('model accuracy')
            plt.ylabel('accuracy')
            plt.xlabel('epoch')
            plt.legend(['train', 'test'], loc='upper left')
            plt.show()
            
            # Summarize history for loss
            plt.plot(history.history['loss'])
            plt.plot(history.history['val_loss'])
            plt.title('model loss')
            plt.ylabel('loss')
            plt.xlabel('epoch')
            plt.legend(['train', 'test'], loc='upper left')
            plt.show()
        except:
            print("Data display failed")
    elif mode == MODE_WARMUP:
        model.save_weights(WARMUP_WEIGHTS_PATH)


def test(images):
    model = keras.models.load_model(SAVE_PATH)
    model.summary()
    
    log = open("output.txt", "w")
    tp = 0
    fp = 0
    tn = 0
    fn = 0
    
    # test
    label = []
    score = [] # disease score
    
    for img in images:
        test_image = keras.preprocessing.image.load_img(img[0], target_size=(IMG_WIDTH, IMG_HEIGHT))
        test_image = keras.preprocessing.image.img_to_array(test_image)
        test_image = test_image.reshape(1, IMG_WIDTH, IMG_HEIGHT, 3)
        test_image = getPreprocessBaseModel(test_image)
        result = model.predict(test_image)
        print(img[1], img[0], result)
        
        # test
        label.append(int(img[1] == "LeafBlast"))
        score.append(result[0][1])
        
        if img[1] == "Healthy":
            if result[0][0] > result[0][1]:
                tn += 1
            else:
                fp += 1
        else:
            if result[0][0] > result[0][1]:
                fn += 1
            else:
                tp += 1
        log.write("{} {} {}\n".format(img[1], "", result))
    log.write("tp {} fp {} tn {} fn {}\n".format(tp, fp, tn, fn))
    log.close()
    
    # test (https://scikit-learn.org/stable/auto_examples/model_selection/plot_roc.html#sphx-glr-auto-examples-model-selection-plot-roc-py)
    print(label)
    print(score)
    fpr, tpr, _ = metrics.roc_curve(label, score,  drop_intermediate=False)
    roc_auc = metrics.auc(fpr, tpr)
    print("AUC:", roc_auc)
    
    plt.figure()
    lw = 2
    plt.plot(fpr, tpr, color='#baa928',
             lw=lw, label='ROC curve (area = %0.4f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='#c47627', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.show()

# dataset-specific workaround to force-use IMG_xxxx images for training and not testing (due to them being zoomed out)
import re
def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)] 
    return sorted(l, key=alphanum_key)

# Setup dataset
train_df = None
validation_df = None
image_list_train = []
image_list_validation = []
try:
    for c in CLASSES:
        dataset_dir = os.path.join(TRAIN_DATA_DIR, c)
        image_list = []
        for root, dirs, files in os.walk(dataset_dir):
            for file in natural_sort(files):
                if file.endswith(".jpg"):
                     image_list.append([os.path.join(root, file), c])
                
        #random.Random(SEED).shuffle(image_list)
        
        # For neural network testing get ~20% of each class
        twenty_percent = round( len(image_list) * 0.2 )
        print("Use", twenty_percent, "of", len(image_list), "for testing")
        image_list_train.extend(image_list[0:-twenty_percent])
        image_list_validation.extend(image_list[-twenty_percent:])

    train_df = pd.DataFrame(image_list_train, columns =['filename', 'class']) 
    validation_df = pd.DataFrame(image_list_validation, columns =['filename', 'class']) 
    #print(train_df)
except:
    print("Dataset cannot be loaded")


##################################################################
## --------------------- PROGRAM SEQUENCE --------------------- ##
##################################################################

# Run "warm-up" otherwise neural network accuracy will suffer
run(mode=MODE_WARMUP, epochs=20)

# Run main training sequence
run(mode=MODE_TRAIN, epochs=20)

# Maybe resume
#run(mode=MODE_RESUME, epochs=50)

# Use neural network on images
test(image_list_validation)

