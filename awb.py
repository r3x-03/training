import matplotlib.pyplot as plt
import cv2
import numpy as np
import math

def load(file):
    pic = cv2.imread(file)  # read file
    pic = cv2.cvtColor(pic, cv2.COLOR_RGB2BGR)  # convert file from RGB to OpenCV's BGR
    return pic

def awb(image):
    #resize the image to 256x256
    size = (256, 256)
    awb_image = cv2.resize(image, size)

    #blur the image for less noise
    awb_image = cv2.GaussianBlur(awb_image, (5,5), 0)

    #convert image to grayscale
    grayscale = cv2.cvtColor(awb_image, cv2.COLOR_BGR2GRAY)

    #only consider pixels brighter than 127 as white
    thresh = cv2.threshold(grayscale, 127, 255, cv2.THRESH_BINARY)[1]

    #displayImage(thresh)

    #get rgb channels of awb frame
    b,g,r = cv2.split(awb_image)

    #get the average r, g, and b of "white" areas of the frame (as determined by threshold)
    counter = 0
    rAvg = 0
    gAvg = 0
    bAvg = 0
    for i in range(0, size[0]):
        for j in range(0, size[1]):
            if thresh[i][j] == 255: #white area
                rAvg += r[i][j]
                gAvg += g[i][j]
                bAvg += b[i][j]
                counter += 1
    rAvg /= counter
    gAvg /= counter
    bAvg /= counter


    #get the average of the r, g, and b values
    #the goal of auto white balance is to make the r, g, and b values equal
    avg_white = (rAvg + gAvg + bAvg) / 3

    #the multipliers needed to equalize the r, g, and b values
    r_multiplier = avg_white / rAvg
    g_multiplier = avg_white / gAvg
    b_multiplier = avg_white / bAvg

    print("rgb multipliers: ")
    print(r_multiplier)
    print(g_multiplier)
    print(b_multiplier)

    #apply rgb multipliers to each channel
    b,g,r = cv2.split(image)
    r = r * r_multiplier
    g = g * g_multiplier
    b = b * b_multiplier

    #remove invalid data
    r = np.clip(r, 0, 255)
    g = np.clip(g, 0, 255)
    b = np.clip(b, 0, 255)

    #convert back to integer
    r = np.uint8(r)
    g = np.uint8(g)
    b = np.uint8(b)

    new = cv2.merge((r, g, b))
    return new

def imageProcess(root, name):
    img = load(os.path.join(root, name))
    #perfrom awb
    img = awb(img)
    
    fname = os.path.join(TRAIN_DATA_DIR_FIXED, name)
    print(fname)
    cv2.imwrite(fname, img)

import os
def awb_loader():
    for root, dirs, files in os.walk(TRAIN_DATA_DIR):
        for name in files:
            if name[-3:] == "jpg":
                imageProcess(root, name)

TRAIN_DATA_DIR = "/home/harvsftw/Desktop/dataset/Healthy"
TRAIN_DATA_DIR_FIXED = "/home/harvsftw/Desktop/dataset-awb/Healthy"
awb_loader()

TRAIN_DATA_DIR = "/home/harvsftw/Desktop/dataset/LeafBlast"
TRAIN_DATA_DIR_FIXED = "/home/harvsftw/Desktop/dataset-awb/LeafBlast"
awb_loader()
